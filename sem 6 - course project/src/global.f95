module GLOBAL_VARIABLES

  integer, parameter :: Nr = 16
  integer, parameter :: Nz = 16

  integer, parameter :: M  = Nr
  integer, parameter :: N  = Nz

  real(8), parameter :: R_max = 4.d0!2.0
  real(8), parameter :: L_max = 8.d0!4.0

end module GLOBAL_VARIABLES
