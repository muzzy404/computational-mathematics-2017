module SCHEME
  contains

  ! r_half(i) = r(i - 1/2), r_half(i + 1) = r(i + 1/2)
  subroutine make_grid(r, z, r_half, z_half, hr, hz)
    use GLOBAL_VARIABLES
    implicit none

    real(8), intent(out) :: hr, hz
    real(8), intent(out), dimension(0:Nr) :: r
    real(8), intent(out), dimension(0:Nz) :: z
    real(8), intent(out), dimension(Nr) :: r_half
    real(8), intent(out), dimension(Nz) :: z_half

    integer :: i

    hr = R_max / dble(Nr)
    hz = L_max / dble(Nz)

    write(*,*) "steps: ", hr, hz

    r(0) = 0.d0
    z(0) = 0.d0

    do i = 1, Nr
      r(i) = i * hr
      r_half = (r(i) - r(i-1)) / 2.d0
    end do

    do i = 1, Nz
      z(i) = i * hz
      z_half = (z(i) - z(i-1)) / 2.d0
    end do

  end subroutine


  subroutine fill_C_F(C, F, V) ! V is the real Y vector
    use GLOBAL_VARIABLES
    use FUNCTIONS

    implicit none

    real(8), intent(out), dimension(M, M)   :: C
    real(8), intent(out), dimension(M, 0:N) :: F, V

    real(8)                  :: hr, hz ! steps
    real(8), dimension(0:Nr) :: r      ! r grid
    real(8), dimension(0:Nz) :: z      ! z grid
    real(8), dimension(Nr)   :: r_half ! r additional grid
    real(8), dimension(Nz)   :: z_half ! z additional grid

    real(8) :: t, e
    integer :: i, j, out_f

    call make_grid(r, z, r_half, z_half, hr, hz)

    t = (hz**2.d0) / (hr**2.d0)
    e =  hz**2.d0

    ! fill C
    i = 0
    j = i + 1

    C(:,:) = 0.d0
    C(j, 1) = t * f_k(r_half(i+1)) + 2.d0  ! 1.d0
    C(j, 2) = -4.d0 * t * f_k(r_half(i+1)) ! 2.d0

    do i = 1, (M - 1)  ! 1 .. (M - 1) -> j = 2 .. M
      j = i + 1
      C(j, j) = t * (1.d0/r(i)) * (r_half(i)   * f_k(r_half(i)) + &
                                   r_half(i+1) * f_k(r_half(i+1))) + 2.d0 ! 3.d0
      C(j, j-1) = -t * (r_half(i)/r(i)) * f_k(r_half(i))                  ! 4.d0
      if (j < M) C(j, j+1) = -t * (r_half(i+1)/r(i)) * f_k(r_half(i+1))   ! 5.d0
    end do

    ! fill F and real Y
    do i = 0, (M - 1)
      do j = 0, N
        V(i+1,j) = f_u(r(i), z(j))
      end do


      F(i+1,0) = f_phi3(r(i)) !3.d0
      F(i+1,N) = f_phi4(r(i)) !4.d0

      if (i < (M - 1)) then
        F(i+1,1)   = e * f_f(r(i), z(1))   + f_phi3(r(i)) !1.d0
        F(i+1,N-1) = e * f_f(r(i), z(N-1)) + f_phi4(r(i)) !2.d0

        do j = 2, (N - 2)
          F(i+1,j) = e * f_f(r(i), z(j)) !8.d0
        end do
      end if
    end do

    ! lower row
    i = M - 1

    j = 1
    F(i+1,j) = e * f_f(r(i), z(j)) + &
               t * (r_half(i+1)/r(i)) * f_k(r_half(i+1)) * f_phi2(z(j)) + &
               f_phi3(r(i)) !5.d0
    j = N-1
    F(i+1,j) = e * f_f(r(i), z(j)) + &
               t * (r_half(i+1)/r(i)) * f_k(r_half(i+1)) * f_phi2(z(j)) + &
               f_phi4(r(i)) !6.d0
    do j = 2, (N-2)
      F(i+1,j) = e * f_f(r(i), z(j)) + &
                 t * (r_half(i+1)/r(i)) * f_k(r_half(i+1)) * f_phi2(z(j)) !7.d0
    end do

    ! print matrix to file
    open(newunit = out_f, file = "C-matrix.dat", status = "replace")
    do i = 1, M
      write(out_f,'(200F9.3)') C(i,:)
    end do
    close(out_f)

end subroutine

end module SCHEME
