module FUNCTIONS
  contains

  function f_u(r, z) result(res)
    implicit none
    real(8) :: r, z, res

    res = 3.d0*(r**2.d0) + (z**3.d0) ! test 1
    !res = 10.d0*(r**4.d0) + (z**4.d0) + 2.d0 ! test 2
  end function

  function f_k(r) result(res)
    implicit none
    real(8) :: r, res

    res = (r**2.d0) + 5.d0 * r ! test 1
    !res = 2.d0*(r**4.d0) + (r**2.d0) + 3.d0*r ! test 2
  end function

  function f_phi2(z) result(res)
    implicit none
    real(8) :: z, res

    res = 1.d0
  end function

  function f_phi3(r) result(res)
    implicit none
    real(8) :: r, res

    res = 1.d0
  end function

  function f_phi4(r) result(res)
    implicit none
    real(8) :: r, res

    res = 1.d0
  end function

  function f_f(r, z) result(res)
    implicit none
    real(8) :: r, z, res

    res = -(3.d0*r + 6.d0*z + 10.d0) ! test 1
    !res = -(10.d0*(r**3.d0) + 3.d0*r + 6.d0 + 12.d0*(z**2.d0)) ! test 2
  end function

end module FUNCTIONS
