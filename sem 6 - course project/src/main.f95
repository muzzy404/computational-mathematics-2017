subroutine tridiagonal_matrix_algorithm(N, matrix, f, v)
  implicit none

  integer, intent(in) :: N

  real(8), dimension(N, N), intent(in)  :: matrix

  real(8), dimension(N), intent(in)  :: f
  real(8), dimension(N), intent(out) :: v

  real(8), dimension(N) :: a, b, c
  real(8), dimension(N) :: alpha, beta
  integer :: i

  a(1) = 0
  c(N) = 0
  do i = 1, N
    if (i < N) then
      a(i+1) = matrix(i+1, i)
      c(i)   = matrix(i, i+1)
    end if
    b(i) = matrix(i, i)
  end do
  !write(*,*) "a = ", int(a(:))
  !write(*,*) "b = ", int(b(:))
  !write(*,*) "c = ", int(c(:))

  alpha(1) = 0
   beta(1) = 0

  do i = 1, (N-1)
    alpha(i + 1) = -c(i)/(a(i)*alpha(i) + b(i))
    beta(i + 1)  = (f(i) - a(i)*beta(i))/(a(i)*alpha(i) + b(i))
  end do

  v(N) = (f(N) - a(N)*beta(N))/(a(N)*alpha(N) + b(N))

  i = N - 1
  do while(i > 0)
    v(i) = alpha(i+1)*v(i+1) + beta(i+1)
    i = i - 1
  end do

end subroutine


subroutine modified_tridiagonal_matrix_algorithm(C, F, Y, M, N)
  implicit none

  real(8), parameter :: PI = 4.d0 * DATAN(1.d0)

  integer, intent(in) :: M, N
  real(8), intent(in),  dimension(M, M) :: C
  real(8), intent(in),  dimension(M, 0:N) :: F
  real(8), intent(out), dimension(M, 0:N) :: Y

  real(8), dimension(M)    :: alphaY
  real(8), dimension(M, N) :: beta

  real(8), dimension(M, M) :: E, matrix
  real(8), dimension(M)    :: V, vector

  real(8) :: a
  integer :: j, k

  Y(:, 0) = F(:, 0)
  Y(:, N) = F(:, N)

  ! Identity matrix
  E = 0.d0
  do j = 1, M
    E(j, j) = 1.d0
  end do

  ! count beta vector
  beta(:, 1) = F(:, 0)
  !write(*,'(A6,I2)') "beta: ", 1
  !write(*,'(10ES10.2)') beta(:, 1)

  do j = 1, (N - 1)
    vector = F(:, j) + beta(:, j)
    !write(*,*) 'vector'
    !write(*,'(40F12.6)') F(:, j), beta(:, j), vector

    beta(:, j+1) = 0.d0
    do k = 1, j !(j - 1 + 1) 'cause alpha(j + 1)
      matrix = C - (2.d0 * cos(k * PI / dble(j + 1)) * E)
      a = (2.d0 * (sin(k * PI / dble(j + 1)))**2) / dble(j + 1)

      ! solve the system, got the V
      call tridiagonal_matrix_algorithm(M, matrix, (a * vector), V)

      beta(:, j+1) = beta(:, j+1) + V
    end do
    !write(*,'(A6,I2)') "beta: ", j+1
    !write(*,'(10ES10.2)') beta(:, j+1)
  end do

  do j = (N - 1), 1, -1
    vector = Y(:, (j+1))

    alphaY(:) = 0.d0
    do k = 1, j !(j - 1 + 1) 'cause alpha(j + 1)
      matrix = C - 2.d0 * cos(k * PI / dble(j + 1)) * E
      a = (2 * (sin(k * PI / dble(j + 1)))**2) / dble(j + 1)

      call tridiagonal_matrix_algorithm(M, matrix, (a * vector), V)

      alphaY = alphaY + V
    end do
    !write(*,'(A8,I2)') "alphaY: ", (j+1)
    !write(*,'(10ES10.2)') alphaY(:)
    Y(:, j) = alphaY + beta(:, (j+1))

    !write(*,'(A2,I3)') "Y:", j
    !write(*,'(10ES10.2)') Y(:, j)
  end do

end subroutine


subroutine test_method()
  implicit none

  integer, parameter :: N = 4
  integer, parameter :: M = 3

  real(8), dimension(M, M)               :: C
  real(8), dimension(M*(N-1), M*(N-1))   :: A

  real(8), dimension(M*(N-1)) :: F_full, Y_full
  real(8), dimension(M, 0:N)  :: F,      Y

  integer :: i, j, k, out_f

  A = 0.d0
  do i = 1, (M*(N-1))
    A(i, i) = 100.d0 !4.d0

    if (i < ((N-1)*M)) then
      A(i, i+1) = 5.d0 ! upper  diagonal
      A(i+1, i) = 5.d0 ! bottom diagonal
    end if

    if ((i+3) <= ((N-1)*M)) then
      A(i, i+3) = -1.d0 ! upper  F diagonal
      A(i+3, i) = -1.d0 ! bottom F diagonal
    end if
  end do

  ! get C
   write(*,*) "C matrix"
  do i = 1, M
    C(i, :) = A(i, 1:M)
    write(*,*) int(C(i,:))
  end do

  ! check A
  write(*,*) "A matrix"
  do i = 1, ((N-1)*M)
    write(*,*) int(A(i,:))
  end do

  !Y_full = 2.d0
  do j = 1, (M*(N-1))
    Y_full(j) = dble(j * 100.d0)
  end do
  F_full = matmul(A, Y_full)

  write(*,*) "F vector"
  write(*,*) int(F_full(:))

  F(:,:) = 0.d0
  k = 1
  do j = 1, (N - 1)
    do i = 1, M
      F(i,j) = F_full(k)
      k = k + 1
    end do
  end do

  F(:,0) = 0.d0 !-1.d0
  F(:,N) = 0.d0 !-1.d0

  write(*,*) "F vectors"
  do i = 0, N
    write(*,'(10ES10.2)') F(:,i)
  end do

  ! TEST
  write(*,*) "real Y"
  write(*,*) int(Y_full(:))

  !Y(:,:) = 0.d0
  !write(*,*) "Y"
  !write(*,*) Y(:,:)

  call modified_tridiagonal_matrix_algorithm(C, F, Y, M, N)
  write(*,*) "SOLUTION"
  do i = 0, N
    write(*,'(3ES10.2)') Y(:,i)
  end do

  open(newunit = out_f, file = "method-test.dat", status = "replace")
  write(*,'(A10,A10,A10)') "real Y|", "new Y|", "error|"
  k = 1
  do i = 1, (N-1)
    do j = 1, M
      write(*,'(3ES10.2)') Y_full(k), Y(j, i), abs(Y_full(k) - Y(j, i))
      write(out_f,*) Y_full(k), Y(j, i), F_full(k)
      k = k + 1
    end do
  end do
  close(out_f)

end subroutine

subroutine tridiagonal_matrix_algorithm_test()
  implicit none

  integer, parameter :: N = 5
  real(8), dimension(N,N) :: matrix
  real(8), dimension(N)   :: real_y, y, f

  integer :: i

  matrix(:,:) = 0.d0
  do i = 1, N
    matrix(i,i) = 3.d0
    if ((i+1) <= N) then
      matrix(i, i+1) = 1.d0
      matrix(i+1, i) = 1.d0
    end if
  end do

  do i = 1, N
    write(*,*) int(matrix(i,:))
  end do

  do i = 1, N
    real_y(i) = dble(i)
  end do
  f = matmul(matrix, real_y)

  call tridiagonal_matrix_algorithm(N, matrix, f, y)

  write(*,*) "y and error"
  do i = 1, N
    write(*,*) y(i), abs(y(i) - real_y(i))
  end do

end subroutine


program main
  USE GLOBAL_VARIABLES
  USE SCHEME

  implicit none

  real(8), dimension(M, M)   :: C
  real(8), dimension(M, 0:N) :: F
  real(8), dimension(M, 0:N) :: Y, V

  integer :: i, j, out_f

  call fill_C_F(C, F, V)

  ! -----------------------------
  !write(*,*) "C matrix"
  !do i = 1, M
  !  write(*,'(20ES10.2)') C(i,:)
  !end do

  !write(*,*)
  !write(*,*) "F vectors"

  !do i = 1, M
  !  write(*,'(20ES10.2)') F(i,:)
  !end do
  ! -----------------------------

  call modified_tridiagonal_matrix_algorithm(C, F, Y, M, N)

  ! -----------------------------
  !write(*,*)
  !write(*,*) "Y vectors"
  !do i = 1, M
  !  write(*,'(20ES10.2)') Y(i,:)
  !end do
  ! -----------------------------

  open(newunit = out_f, file = "Y.dat", status = "replace")
    do j = 1, (N-1) !0, N
      do i = 1, M
        write(out_f,*) V(i,j), Y(i,j), F(i,j), abs(V(i,j) - Y(i,j))
        write(*,'(3F12.6)') V(i,j), Y(i,j), abs(V(i,j) - Y(i,j))
      end do
    end do
  close(out_f)

  !call test_method()
  !call tridiagonal_matrix_algorithm_test
end
