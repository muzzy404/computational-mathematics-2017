module FUNCTIONS_MODULE

contains

function k_function(r) result (k)
  implicit none
  real(8), intent(in) :: r
  real(8)             :: k

  ! test 1
  !k = 2.0*r + 1.0

  ! test 2
  !k = 5.0*r**2 + 2.0*r

  ! test 3
  k = 2.0*r + 1.0
end function

function q_function(r) result (q)
  implicit none
  real(8), intent(in) :: r
  real(8)             :: q

  ! test 1
  !q = 1.0

  ! test 2
  !q = 2.0*r

  ! test 3
  q = r
end function

function f_function(r) result (f)
  implicit none
  real(8), intent(in) :: r
  real(8)             :: f

  ! test 1
  !f = -(12.0*r + 4 - r**2)

  ! test 2
  !f = -(45.0*r + 12 - 6*r**2)

  ! test 3
  f = 5.0*r
end function

function u_function(r) result (u)
  implicit none
  real(8), intent(in) :: r
  real(8)             :: u

  ! test 1
  !u = r**2

  ! test 2
  !u = 3.0*r

  ! test 3
  u = 5
end function

function w_function(r) result (w)
  implicit none
  real(8), intent(in) :: r
  real(8)             :: w

  ! test 1
  !w = -2.0*r*(2.0*r + 1.0)

  ! test 2
  !w = -3.0*(5.0*r**2 + 2.0*r)

  ! test 3
  w = 0
end function

end module FUNCTIONS_MODULE
