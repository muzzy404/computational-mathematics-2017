! Lab work 01

subroutine tridiagonal_matrix_algorithm(N, a, b, c, f, v)
  implicit none

  integer, intent(in) :: N

  real(8), dimension(N), intent(in)  :: a, b, c, f
  real(8), dimension(N), intent(out) :: v

  real(8), dimension(N) :: alpha, beta
  integer :: i

  alpha(1) = 0
   beta(1) = 0

  do i = 1, (N-1)
    alpha(i + 1) = -c(i)/(a(i)*alpha(i) + b(i))
    beta(i + 1)  = (f(i) - a(i)*beta(i))/(a(i)*alpha(i) + b(i))
  end do

  v(N) = (f(N) - a(N)*beta(N))/(a(N)*alpha(N) + b(N))

  i = N - 1
  do while(i > 0)
    v(i) = alpha(i+1)*v(i+1) + beta(i+1)
    i = i - 1
  end do

end subroutine

! for example: r(i + 1/2) = r_half(i)
!              r(i - 1/2) = r_half(i - 1)
subroutine get_grid(N, begin_val, end_val, arr, arr_half, step, step_half)
  implicit none

  integer, intent(in)  :: N
  real(8), intent(in)  :: begin_val, end_val

  real(8), dimension(N),   intent(out) :: arr
  real(8), dimension(N-1), intent(out) :: arr_half

  real(8), intent(out) :: step, step_half

  integer :: i, M

  M = (2 * N) - 1

  step      = (end_val - begin_val)/(N - 1)
  step_half = (end_val - begin_val)/(M - 1)

  do i = 1, N
    arr(i) = begin_val + (i-1)*step
  end do

  do i = 1, (N - 1)
    !arr_half(i) = begin_val + step_half + (i-1) * step
    !arr_half(i) = begin_val + (2*i - 1)*step_half
    arr_half(i) = begin_val + arr(i) + step_half
  end do

end subroutine

subroutine get_kqf_vectors(N, r, r_half, &             ! in
                           k_half_arr, q_arr, f_arr, & ! out
                           k, q, f)                    ! functions
  implicit none

  integer, intent(in) :: N

  real(8), dimension(N),   intent(in) :: r
  real(8), dimension(N-1), intent(in) :: r_half

  real(8), dimension(N),   intent(out) :: q_arr, f_arr
  real(8), dimension(N-1), intent(out) :: k_half_arr

  real(8)    :: k, q, f ! functions
  integer :: i

  do i = 1, N
    if (i < N) then
      k_half_arr(i) = k(r_half(i))
    end if

    q_arr(i) = q(r(i))
    f_arr(i) = f(r(i))
  end do

end subroutine

subroutine get_abc_coefficients(N, r, r_half, & ! in
                                k_half, q, f, & ! in
                                h, h_half, w, & ! in
                                a, b, c, g)     ! out
  implicit none

  ! in parameters
  integer, intent(in) :: N

  real(8), dimension(N),   intent(in) :: r, q, f
  real(8), dimension(N-1), intent(in) :: r_half, k_half

  real(8), intent(in) :: h, h_half, w

  ! out parameters
  real(8), dimension(N), intent(out) :: a, b, c, g

  integer :: i

  a(1) = 0
  c(N) = 0

  i = 1
  c(i) = -(2*r_half(i)*k_half(i))/(h)
  b(i) = h_half*q(i)*r_half(i) + (-c(i))
  g(i) = f(i)*h_half*r_half(i)

  i = N
  a(i) = -(r_half(i-1)*k_half(i-1))/(h)
  b(i) = h_half*q(i)*r(i) + (-a(i))
  g(i) = r(i)*(f(i)*h_half - w)

  do i = 2, (N-1) ! h_half -> h from 2 to (N-1)
    a(i) = -(r_half(i-1)*k_half(i-1))/(h)
    c(i) = -(r_half(i)*k_half(i))/(h)
    b(i) = h*q(i)*r(i) + (-a(i)) + (-c(i))
    g(i) = f(i)*h*r(i)
  end do

end subroutine

program main
  !use TESTS_MODULE
  use FUNCTIONS_MODULE

  implicit none

  integer, parameter :: N = 64 ! number of points
  integer :: i, out_f

  real(8), dimension(N)   :: r, q, f, a, b, c, g, v, discrepancy
  real(8), dimension(N-1) :: r_half, k_half

  real(8) :: h, h_half, w, beg_r, rad
  real(8) :: err_loc, err_max

  beg_r = 0.0
  rad   = 2.0

  !boundary
  w = w_function(rad)

  call get_grid(N, beg_r, rad, r, r_half, h, h_half)
  call get_kqf_vectors(N, r, r_half, k_half, q, f, k_function, q_function, f_function)
  call get_abc_coefficients(N, r, r_half, k_half, q, f, h, h_half, w, a, b, c, g)

  call tridiagonal_matrix_algorithm(N, a, b, c, g, v)

  open(newunit = out_f, file = "results.dat", status = "replace")
  do i = 1, N
    write(*,'(F16.10,A3,F25.15,A3,F25.15)') &
                   ((i-1)*h), '   ', v(i), '   ', u_function((i-1)*h)
    write(out_f,'(F16.10,A3,F25.15,A3,F25.15)') &
                   ((i-1)*h), '   ', v(i), '   ', u_function((i-1)*h)
    !write(out_f,*) ((i-1)*h),        v(i),        u_function((i-1)*h)
  end do
  write(*,*)
  close(out_f)

  discrepancy(1) = (f_function(beg_r) * h_half * h) -                      &
             (b(1)*u_function(beg_r)   + c(1)*u_function(beg_r + h))
  discrepancy(N) = ((h*(N-1)) * (f_function(h*(N-1))*h_half - w)) -        &
             (a(N)*u_function(h*(N-2)) + b(N)*u_function(h*(N-1)  ))

  do i = 2, (N-1)
    discrepancy(i) = (f_function(h*(i-1)) * h * (h*(i-1))) - &
               (a(i)*u_function(h*(i-2)) +             &
                b(i)*u_function(h*(i-1)) +             &
                c(i)*u_function(h*i   ))
  end do

  ! let's find discrepancy and |u - v|
  write(*,*) 'ERROR: discrepancy, |u - v|'
  err_max = 0.0

  do i = 1, N
    err_loc = abs(u_function(h*(i-1)) - v(i))

    write(*,'(I4,A3,F25.15,F25.15)') i, '   ', discrepancy(i), err_loc

    ! find max error
    if (err_loc > err_max) then
      err_max = err_loc
    end if

  end do
  write(*,*)

  write(*,'(A4,F9.6,A10,F9.6)') 'h = ', h, ' h_half = ', h_half
  write(*,'(A12,ES12.4)') 'max error = ', err_max
  write(*,*)

  write(*,*) 'ALL DONE!'
end
