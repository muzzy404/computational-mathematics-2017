module TESTS_MODULE

contains

subroutine test_r_grid()
  use FUNCTIONS_MODULE
  implicit none

  integer, parameter :: N = 11

  real(8), dimension(N)   :: r, q, f
  real(8), dimension(N-1) :: r_half, k_half

  real(8) :: h, h_half

  integer :: i

  call get_grid(N, 0.0, 100.0, r, r_half, h, h_half)
  call get_kqf_vectors(N, r, r_half, k_half, q, f, k_function, q_function, f_function)

  do i = 1, N
    write(*,'(I2,A,F7.3,A,F7.2,A,F7.2)') i, '      ', r(i), ' q = ', q(i), ' f = ', f(i)
  end do
  write(*,*) '------------------------------'
  do i = 1, (N-1)
    write(*,'(I2,A,F7.3,A,F7.2)') i, '+ 1/2 ', r_half(i), ' k = ', k_half(i)
  end do

end subroutine

subroutine get_abc_from_matrix(M, N, a, b, c)
  implicit none

  integer,               intent(in)  :: N
  real(8), dimension(N, N), intent(in)  :: M
  real(8), dimension(N),    intent(out) :: a, b, c

  integer :: i

  a(1) = 0
  c(N) = 0

  do i = 2, N
    a(i) = M(i-1, i)
  end do
  do i = 1, N
    b(i) = M(i, i)
  end do
  do i = 1, (N-1)
    c(i) = M(i+1, i)
  end do

end subroutine

subroutine test_data(a, b, c, f, v)
  implicit none

  integer, parameter :: N = 4
  integer :: i

  real(8), dimension(N, N) :: matrix

  real(8), dimension(N), intent(out) :: v
  real(8), dimension(N), intent(out) :: a, b, c, f

  matrix = reshape((/ 2.0,  -1.0,   0.0,   0.0,    &
                     -1.0,   4.0,  -2.0,   0.0,    &
                      0.0,  -2.0,   6.0,  -3.0,    &
                      0.0,   0.0,  -3.0,   4.0  /),&
                    shape(matrix), order = (/2,1/))
  v = (/ 1.0,  2.0,  3.0,  4.0/)

  call get_abc_from_matrix(matrix, N, a, b, c)

  do i = 1, N
    write(*,*) matrix(i,:)
  end do

  f = matmul(matrix, v)

  write(*,*) '-------------------------------------'
  write(*,*) 'V   = ', v

end subroutine

subroutine algorithm_test()
  implicit none
  integer, parameter :: N = 4
  integer            :: i

  real(8), dimension(N) :: a, b, c, f, v, v_ref

  call test_data(a, b, c, f, v_ref)

  write(*,*) '-------------------------------------'
  write(*,*) 'F   = ', f
  write(*,*) '-------------------------------------'
  write(*,*) 'a   = ', a
  write(*,*) 'b   = ', b
  write(*,*) 'c   = ', c
  write(*,*) '-------------------------------------'

  call tridiagonal_matrix_algorithm(N, a, b, c, f, v)

  write(*,*) 'res = ', v

  do i = 1, N
    write(*,'(ES12.4)') (v_ref(i) - v(i))
  end do

end subroutine


end module TESTS_MODULE
