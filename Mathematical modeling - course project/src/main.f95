subroutine quadratic_equation(p5_1, p5_2, x2, p4)
  implicit none

  real, intent(out) :: p5_1, p5_2
  real, intent(in)  :: x2, p4

  real :: a, b, c, m, D

  m = 400 / (20 + x2)**2

  a = (m * (x2**2)) / p4
  b = (2 * (m/p4) * x2**2 - m * x2 + 1)
  c = (    (m/p4) * x2**2 - m * x2 + 1)

  D = b**2 - 4*a*c

  p5_1 = (-b - sqrt(D))/(2 * a)
  p5_2 = (-b + sqrt(D))/(2 * a)

  !write(*,*) "a = ", a
  !write(*,*) "b = ", b
  !write(*,*) "c = ", c
  !write(*,*) "D = ", D

end subroutine

real function check_det_2x2(x1, x2, p2, p4, p5) result(det)
  implicit none

  real, intent(in)      :: x1, x2, p2, p4, p5
  real, dimension(2, 2) :: J

  J(1,1) = -1 - p2      * exp((20 * x2) / (20 + x2))
  J(2,1) = -1 * p2 * p4 * exp((20 * x2) / (20 + x2))

  J(1,2) = ((400 * p2 *      (1 - x1)) / (20 + x2)**2) * exp((20 * x2) / (20 + x2))
  J(2,2) = ((400 * p2 * p4 * (1 - x1)) / (20 + x2)**2) * exp((20 * x2) / (20 + x2)) - p5 - 1

  det = J(1,1) * J(2,2) - J(2,1) * J(1,2)
end function

program main
  implicit none
  integer :: re_i, i, out_f

  real :: p5_1, p5_2, p2
  real :: x2_start, x2_stop, x2, x2_glob, x1, step
  real :: Jdet, Jdet_p2, Jdet_p5
  real :: check_1, check_2
  real :: check_det_2x2

  real,          dimension(4) :: p4
  character(LEN=15), dimension(4) :: res_file

  step     = 0.001!0.001
  x2_start = 1.15!1.0
  x2_stop  = 10.0 + step
  !x2_stop  = 20.0 + step

  p4 = (/6.0, 8.0, 10.0, 12.0/)
  res_file(1) = "p5,p2-p4--6.txt";
  res_file(2) = "p5,p2-p4--8.txt";
  res_file(3) = "p5,p2-p4-10.txt";
  res_file(4) = "p5,p2-p4-12.txt";

  do i = 1, 4
  write(*,"(A5,F5.2)") "p4 = ", p4(i)

    x2_glob = x2_start
    open(newunit = out_f, file = res_file(i), status = "replace")

    do while (x2_glob < x2_stop)
      x2      = x2_glob
      x2_glob = x2_glob + step

      call quadratic_equation(p5_1, p5_2, x2, p4(i))

      x1 = x2 * (p5_2 + 1)/p4(i)
      p2 = x1 / ((1 - x1) * exp((20 * x2)/(20 + x2)))

      ! because p2,p5 > 0
      if ((p2 < 0) .or. (p5_2 < 0)) cycle

      check_1 = -x1 + p2 *         (1 - x1) * exp((20 * x2)/(20 + x2))
      check_2 = -x2 + p2 * p4(i) * (1 - x1) * exp((20 * x2)/(20 + x2)) - p5_2 * x2

      Jdet_p2 = -p4(i) * (1 - x1) * exp((20 * x2)/(20 + x2))
      Jdet_p5 = (-1 - p2 * exp((20 * x2)/(20 + x2))) * (-x2)

      Jdet = check_det_2x2(x1, x2, p2, p4(i), p5_2)


      !write(out_f,"(F10.6, F10.6, F10.6, F10.6, F10.6, F12.4,   F12.4,   F12.4, F12.4,   F12.4)") &
      write(out_f,"(F10.6, F10.6, F10.6, F10.6, F10.6, F12.6,   F12.6,   ES14.3, ES14.3,  ES14.2)") &
                    x2,    x1,    p5_1,  p5_2,  p2,    Jdet_p2, Jdet_p5, Jdet,   check_1, check_2

    end do

    close(out_f)
    write(*,*) res_file(i), " - ready"
    write(*,*) "============================="
  end do

  re_i = system("pause")
end
