MODULE DATA_MODULE

CONTAINS

subroutine solve_data_system(R, R2, e2)
  use DECOMP_MODULE
  use SOLVE_MODULE

  implicit none
  real, intent(out) :: R, R2, e2

  integer, parameter :: N =3

  real, dimension(N, N) :: matrix
  real, dimension(N)    :: b
  ! for DECOMP and SOLVE
  integer, dimension(N) :: index_vector
  real,    dimension(N) :: work_array
  real                  :: cond

  ! fill data
  matrix = reshape((/ 53, 46, 20,    &
                      46, 50, 26,    &
                      20, 26, 17 /), &
           shape(matrix), order = (/2, 1/))
  b = (/3060, 2866, 1337/)

  call DECOMP(N, N, matrix, cond, index_vector, work_array)
  call SOLVE(N, N, matrix, b, index_vector)

  R  = nint(b(1))
  R2 = nint(b(2))
  e2 = nint(b(3))

  ! checking answers
  !write (*,*) 'R  = ', R
  !write (*,*) 'R2 = ', R2
  !write (*,*) 'e2 = ', e2

end subroutine

function L_function(x) result (res)
  implicit none
  real, intent(in) :: x
  real :: res
  res = cos(x) / x
end function

function find_L() result(L)
  use QUANC8_MODULE

  implicit none
  integer :: no_fun
  real :: L, err_est, flag
  real :: re_err = 1.e-06

  call QUANC8(L_function, 1.0, 2.0, 0.0, re_err, &
              L, err_est, no_fun, flag)
  ! L result checking
  !write(*,*) '    Integral value   = ', L
  !write(*,*) '    Error estimation = ', err_est
  !write(*,*) '    Num functions    = ', no_fun
  !write(*,*) '    Flag             = ', flag

  L = 0.4674158 * L
end function

real function equation(x) result(f)
  implicit none
  real, intent(in) :: x

  f = x - 0.6**x
end function equation

function find_e1_by_bisection() result(e1)
  implicit none
  real :: e1, x, a, b

  real :: res_err = 1.e-08
  a = 0.6
  b = 0.8

  do
    x = (a + b) / 2
    e1 = 0.6**x

    if ((e1 < (x + res_err)).and.(e1 > (x - res_err))) exit

    a = e1
    b = x
  end do
  !write(*,*) "E = ", e1
  e1 = 5.718088 * e1
  !write(*,*) "E1 = ", e1
end function

END MODULE DATA_MODULE
