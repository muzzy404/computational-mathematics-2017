module SHARED_VARIABLES
  real     :: R, R2, E2, E1, L, C_global
  save        R, R2, E2, E1, L, C_global

  integer :: check
  save       check
end module SHARED_VARIABLES

subroutine find_data(R, R2, E2, E1, L)
  use DATA_MODULE
  use ZEROIN_MODULE
  implicit none

  real :: R, R2, E2, E1, L
  real :: A, B, TOL

  A = 0.6
  B = 0.8
  TOL = 1.0e-8

  call solve_data_system(R, R2, E2)
  L  = find_L()
  E1 = ZEROIN(A, B, equation, TOL) ! or use find_e1_by_bisection()

  !write(*,*) "E = ", e1
  E1 = 5.718088 * E1
  !write(*,*) "E1 = ", e1

  !             R       R2      E2      E1      L
  write(*,"(A10,A3,F4.1,A5,F4.1,A5,F4.1,A5,F4.1,A5,E10.3)") &
          "FindData: ", "R=",R,"  R2=",R2,"  E2=",E2,"  E1=",E1,"  L=",L
end subroutine

subroutine diff_system(t, y, yp)
  use SHARED_VARIABLES

  implicit none
  ! i1 = y(1), i3 = y(2), Uc = y(3)
  real :: L1, L3, R1, R3
  real :: t ! right part do not include t
  real, dimension (3) :: y, yp

  L1 = L
  L3 = L
  R1 = R
  R3 = R

  yp(1) = (1/L1) * (E1 - E2 - y(3) + y(2)*R2 - y(1)*(R1 + R2))
  yp(2) = (1/L3) * (E2 + y(3) + y(1)*R2 - y(2)*(R2 + R3))
  yp(3) = (1/C_global)*(y(1) - y(2))

  if (check == 1) then
    !             R       R2      E2      E1      L        C
    write(*,"(A10,A3,F4.1,A5,F4.1,A5,F4.1,A5,F4.1,A5,E10.3,A5,ES12.6,F12.7)") &
    "System: ", "R=",R,"  R2=",R2,"  E2=",E2,"  E1=",E1,"  L=",L," C=", C_global, C_global

    check = 0
    write(*,*) "-------------"
  end if

end subroutine

real function function_C(C_val) result (res)
  use RKF45_MODULE
  use SHARED_VARIABLES

  implicit none
  real, intent(in) :: C_val

  external :: diff_system
  ! RKF variables
  integer, parameter :: neqn = 3
  integer :: iflag, k
  real :: t, tout, relerr, abserr, tfinal, tprint, sub
  real, dimension(neqn) :: x
  real, dimension(11)   :: Uc_exp, Uc
  real, dimension(3 + 6 * neqn) :: work
  integer, dimension(5) :: iwork

  Uc_exp = (/-1.000,  7.777, 12.017, 10.701, &
              5.407, -0.843, -5.159, -6.015, &
              -3.668, 0.283,  3.829/)

  check = 1
  !             R       R2      E2      E1      L        C
  write(*,"(A10,A3,F4.1,A5,F4.1,A5,F4.1,A5,F4.1,A5,E10.3,A5,ES12.3)") &
  "F(C): ", "R=",R,"  R2=",R2,"  E2=",E2,"  E1=",E1,"  L=",L," C=", C_val
  C_global = C_val

  t      = 0.0
  tfinal = 1.05e-3    ! 1.0
  tprint = 0.1e-3
  iflag  = 1

  ! value at point t
  x(1) = E1/R ! i1(0) = E1/R
  x(2) = 0    ! i3(0) = 0
  x(3) = -E2  ! Uc(0) = -E2
  ! error
  relerr = 0.1e-04
  abserr = 0.0

  tout = t
  k    = 1
  do while (tout < tfinal) ! 11 times
    call RKF45(diff_system, neqn, x, t, tout, &
               relerr, abserr, iflag, work, iwork)

    write(*,"(A7,ES10.3,I3,A10,I1)") 'tout = ', tout, k, ". iflag = ", iflag

    iflag = 2
    tout  = t + tprint
    Uc(k) = x(3)
    k     = k + 1
  end do
  write(*,*) "============="

  res = 0
  tout = 0.0
  do k = 1, 11
    sub = (Uc(k) - Uc_exp(k))**2
    res = res + sub
                                            !ES12.6)") &
    write(*,"(A5,ES12.3,A10,F6.3,A8,F6.3,A8,F12.10)") &
      "t = ", tout, "Uc_exp = ", Uc_exp(k), " Uc = ", Uc(k), " sub = ", sub
    tout = tout + tprint
  end do
  write(*,"(A8,ES12.6)") "F(C) = ", res

end function function_C

subroutine check_function_C()
  implicit none
  integer :: out_f
  real    :: x    = 0.5e-6
  real    :: step = 0.1e-08!0.001e-6
  real    :: f, function_C

  open(newunit = out_f, file = "check_function.dat", status = "replace")
  do while (x < (2.0e-6 + step))
    f = function_C(x)
    write(out_f,*) x, f
    x = x + step
  end do

end subroutine

program main
  use FMIN_MODULE
  use SHARED_VARIABLES

  implicit none
  integer  :: re_i

  external :: function_C
  real     :: function_C, C

  real :: a, b, eps

  call find_data(R, R2, E2, E1, L)
  !             R       R2      E2      E1      L
  write(*,"(A10,A3,F4.1,A5,F4.1,A5,F4.1,A5,F4.1,A5,E10.3)") &
  "MainData: ", "R=",R,"  R2=",R2,"  E2=",E2,"  E1=",E1,"  L=",L

  ! find C
  a   = 0.5e-06
  b   = 2.0e-06
  eps = 0.1e-07!0.1e-06
  C = FMIN(a, b, function_C, eps)
  !C = FMIN(0.0, 3.5, function_C, 0.05)
  write(*,*) '-------------------------------'
  write(*,"(A5,ES12.6)") "C = ", C
  write(*,"(A5,F12.7)") "C = ", C

  !call check_function_C()

  re_i = system("pause")
end
