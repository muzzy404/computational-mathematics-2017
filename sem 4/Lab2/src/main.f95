subroutine fill_data(matrix)
  implicit none

  real, intent(out), dimension(8, 8) :: matrix

  matrix = reshape((/ 0,   6,  -6,  -4,  -3,  -8,  -5,   5,    &
                      6, -13,  -3,   5,   4,   3,   1,   7,    &
                      5,  -5,  -1,   7,   2,   0,   7,   1,    &
                      5,  -5,   5,   6,   4,  -7,   4,   0,    &
                      4,   4,   7,  -4,   9,  -8,  -8,  -4,    &
                     -4,   5,  -4,   1,   0,  12,   0,   6,    &
                     -3,  -2,  -4,   2,  -8,  -3,  16,   4,    &
                      7,   5,   0,   2,   0,  -6,   8,  -12 /),&
                               shape(matrix), order = (/2,1/))
end subroutine

subroutine write_matrix(matrix, N, M)
  implicit none

  integer, intent(in) :: N, M
  real, intent(in), dimension(N, M) :: matrix
  integer :: i

  write(*,*) '*****************************************'
  do i = 1, N
    write(*,"(20F10.1)") matrix(i,:)
  end do
  write(*,*) '*****************************************'
end subroutine

subroutine calculate_delta(y1, y2, N, M)
  implicit none

  integer, intent(in) :: N, M
  real, intent(in), dimension(N, M) :: y1, y2
  integer :: i, out_f

  open(newunit = out_f, file = "delta.dat", status = "replace")
  do i = 1, N
    write(out_f,"(A6,I1,A3,E12.5)") 'delta_', i, ' = ', &
                                    (norm2(y1(i,:)-y2(i,:)) / norm2(y1(i,:)))
  end do
  close(out_f)

end subroutine

subroutine find_x_1(x)
  use DECOMP_MODULE
  use SOLVE_MODULE

  implicit none

  integer i, k, out_f
  integer, parameter :: N = 8
  real, intent(out), dimension(5, N) :: x

  real, dimension(N, N) :: A
  real, dimension(N)    :: b
  real, dimension(5)    :: p
  ! for DECOMP
  integer, dimension(N) :: index_vector
  real,    dimension(N) :: work_array
  real :: cond

  p = (/1.0, 0.1, 0.01, 0.0001, 0.000001/)

  open(newunit = out_f, file = "results_x1.dat", status = "replace")
  do i = 1, 5
    call fill_data(A)
    b = (/0, 133, 110, 112, 17, 32, 13, -18/)
    A(1,1) = p(i) - 29
    b(1)   = p(i) * 4 - 175

    call DECOMP(N, N, A, cond, index_vector, work_array)
    call SOLVE(N, N, A, b, index_vector)
    x(i,:) = b

    write(out_f,"(A8,F10.6,A10,F15.1)") 'for p = ', p(i), ' cond = ', cond
    write(out_f,*) '----------------------------------------'
    do k = 1, 8
      write(out_f,"(A2,I1,A3,F15.10,A3,F8.1)") 'x_', k, ' = ', b(k), ' ~ ', b(k)
    end do
    write(out_f,*) '========================================'
  end do

  close(out_f)
end subroutine

subroutine find_x_2(x)
  use DECOMP_MODULE
  use SOLVE_MODULE

  implicit none

  integer i, k, out_f
  integer, parameter :: N = 8
  real, intent(out), dimension(5, N) :: x

  real, dimension(N, N) :: A, AT
  real, dimension(N)    :: b, bT
  real, dimension(5)    :: p
  ! for DECOMP
  integer, dimension(N) :: index_vector
  real,    dimension(N) :: work_array
  real :: cond

  p = (/1.0, 0.1, 0.01, 0.0001, 0.000001/)

  open(newunit = out_f, file = "results_x2.dat", status = "replace")
  do i = 1, 5
    call fill_data(A)
    b = (/0, 133, 110, 112, 17, 32, 13, -18/)
    A(1,1) = p(i) - 29
    b(1)   = p(i) * 4 - 175

    AT = transpose(A)
    bT = matmul(AT, b)
    AT = matmul(AT, A)

    call DECOMP(N, N, AT, cond, index_vector, work_array)
    call SOLVE(N, N, AT, bT, index_vector)
    x(i,:) = bT

    write(out_f,"(A8,F10.6,A10,F15.1)") 'for p = ', p(i), ' cond = ', cond
    write(out_f,*) '----------------------------------------'
    do k = 1, 8
      write(out_f,"(A2,I1,A3,F15.10,A3,F8.1)") 'x_', k, ' = ', bT(k), ' ~ ', bT(k)
    end do
    write(out_f,*) '========================================'
  end do

  close(out_f)
end subroutine

program main
  implicit none
  integer :: re_i
  real, dimension(5, 8) :: x1, x2

  call find_x_1(x1)
  call find_x_2(x2)
  call calculate_delta(x1, x2, 5, 8)

  re_i = system("pause")
end program
