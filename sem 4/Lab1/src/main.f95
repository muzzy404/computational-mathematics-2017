function lagrangePolynomial(x_array, y_array, max_index, x) result(res)
  integer, intent(in) :: max_index
  real, intent(in), dimension(0:max_index) :: x_array, y_array
  real, intent(in) :: x

  real :: res, summand
  integer :: i, j
  res = 0.0

  do i = 0, max_index
    summand = 1.0
    do j = 0, max_index
      if (i /= j) then
        summand = summand * ( (x - x_array(j)) / (x_array(i) - x_array(j)) )
      end if
    end do
    res = res + summand * y_array(i)
  end do

end function lagrangePolynomial

function integrandFunc_1(x) result(res)
  real, intent(in) :: x
  real :: res
  res = (abs(x - tan(x))) ** (-1)
  !res = sin(x)
end function

function integrandFunc_2(x) result(res)
  real, intent(in) :: x
  real :: res
  res = (abs(x - tan(x))) ** (-0.5)
end function

program main
  use SPLINE_MODULE
  use SEVAL_MODULE
  use QUANC8_MODULE

  implicit none
  integer re_i

!-------------------------------------------------------
  ! task 1 variables
  real function_value, lagrange_value, spline_value, x_value
  integer, parameter :: N = 10
  real, dimension(0:N) :: x, y, b, c, d
  real, external :: lagrangePolynomial

  !task 2 variables
  real a_val, b_val, re_err, abs_err, &
       integral_res, err_est, flag
  integer no_fun
  real, external :: integrandFunc_1
  real, external :: integrandFunc_2

!-------------------------------------------------------
  integer :: k, out_f

  open(newunit = out_f, file = "results.dat", status = "replace")

  do k = 0, N
    x(k) = 0.1 * k
    y(k) = 1 / (1 + x(k))
  end do

  ! compute coefficients for spline
  call SPLINE(N + 1, x, y, b, c, d)

  do k = 0, 9
    x_value = 0.05 + k * 0.1

    function_value = 1 / (1 + x_value)
    lagrange_value = lagrangePolynomial(x, y, N, x_value)
    spline_value   = SEVAL(N + 1, x_value, x, y, b, c, d)

    ! writing to console
    write(*,'(A,F5.3)') 'x = ', x_value
    write(*,*) 'function = ', function_value
    write(*,*) 'lagrange = ', lagrange_value, abs(function_value - lagrange_value)
    write(*,*) 'spline   = ', spline_value,   abs(function_value - spline_value  )
    write(*,*) '---------------------------------------------'

    ! writing to files
    write(out_f, *) x_value, function_value, lagrange_value, spline_value
  end do

  close(out_f)
!-------------------------------------------------------

  a_val   = 2.0
  b_val   = 5.0
  re_err  = 1.e-06
  abs_err = 0.0

  call QUANC8(integrandFunc_1, a_val, b_val, abs_err, re_err, &
              integral_res, err_est, no_fun, flag)
  write(*,*) 'm = -1.0'
  write(*,*) '    Integral value   = ', integral_res
  write(*,*) '    Error estimation = ', err_est
  write(*,*) '    Num functions    = ', no_fun
  write(*,*) '    Flag             = ', flag
  write(*,*) '----------------------------------------'

  call QUANC8(integrandFunc_2, a_val, b_val, abs_err, re_err, &
              integral_res, err_est, no_fun, flag)
  write(*,*) 'm = -0.5'
  write(*,*) '    Integral value   = ', integral_res
  write(*,*) '    Error estimation = ', err_est
  write(*,*) '    Num functions    = ', no_fun
  write(*,*) '    Flag             = ', flag

  re_i = system("pause")
end

