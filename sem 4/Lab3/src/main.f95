
subroutine diff_system(t, y, yp)
  implicit none

  integer, parameter :: N = 2
  real :: t
  real, dimension (N) :: y, yp

  yp(1) = -71 * y(1) - 70 * y(2) + exp(1 - t**2)
  yp(2) = y(1) + sin(1 - t)

end subroutine


subroutine solve_by_RKF45(x0_1, x0_2)
  use RKF45_MODULE

  implicit none
  real, intent(in) :: x0_1, x0_2
  integer  :: out_f
  external :: diff_system

  integer, parameter :: neqn = 2
  integer :: iflag
  real :: t, tout, relerr, abserr, tfinal, tprint
  real, dimension(neqn) :: x
  real, dimension(3 + 6 * neqn) :: work
  integer, dimension(5) :: iwork

  t      = 0.0
  tfinal = 4.1
  tprint = 0.0002
  iflag  = 1
  ! value at point t
  x(1) = x0_1!0.0 ! -0.95
  x(2) = x0_2!1.0
  ! error
  relerr = 0.1e-04
  abserr = 0.0

  tout = t
  open(newunit = out_f, file = "results.dat", status = "replace")
  do while (tout <= tfinal)
    call RKF45(diff_system, neqn, x, t, tout, &
               relerr, abserr, iflag, work, iwork)

    write(*,"(A7,F4.1,A6,F4.1,A10,I2)") 'tout = ',tout,', t = ',t,', iflag = ',iflag
    write(*,*) '--------------------------------'
    write(*,*) x(:)
    write(*,*) '================================'
    write(*,*)

    write(out_f,*) tout, x(1), x(2)

    iflag = 2
    tout  = t + tprint
  end do
  close(out_f)
end subroutine

subroutine my_RK_method(h, Tn, Zn, RES)
  implicit none

  integer, parameter :: N = 2
  real :: h, Tn
  real, dimension(N)   :: f, Zn, RES
  real, dimension(6,N) :: K
  ! 1 -----------------------
  call diff_system(Tn, Zn, f)
  K(1,:) = h*f(:)
  ! 2 -----------------------
  call diff_system((Tn + h/3), (Zn + K(1,:)/3), f)
  K(2,:) = h*f(:)
  ! 3 -----------------------
  call diff_system((Tn + 0.4*h), (Zn + 0.16*K(1,:) + 0.24*K(2,:)), f)
  K(3,:) = h*f(:)
  ! 4 -----------------------
  call diff_system((Tn + h), (Zn + 0.25*K(1,:) + 3*K(2,:) + 3.75*K(3,:)), f)
  K(4,:) = h*f(:)
  ! 5 -----------------------
  call diff_system((Tn + 2*h/3),&
       (Zn + (6*K(1,:) + 90*K(2,:) - 50*K(3,:) + 8*K(4,:))/81), f)
  K(5,:) = h*f(:)
  ! 6 -----------------------
  call diff_system((Tn + 4*h/5),&
       (Zn + (6*K(1,:) + 36*K(2,:) + 10*K(3,:) + 8*K(4,:))/75), f)
  K(6,:) = h*f(:)

  RES = Zn + (23*K(1,:) + 125*K(2,:) - 81*K(5,:) + 125*K(4,:))/192

end subroutine

subroutine solve_by_my_RK_method()
  implicit none

  integer :: out_f
  integer, parameter :: N = 2
  real, dimension(N) :: Xn, X
  real :: h, t

  h = 0.003!0.0002
  t = 0.0
  Xn = (/0.0, 1.0/)

  open(newunit = out_f, file = "my_method.dat", status = "replace")
  write(out_f,*) t, Xn(1), Xn(2)
  do while (t < 4.1)
    call my_RK_method(h, t, Xn, X)
    t = t + h
    write(out_f,*) t, X(1), X(2)
    Xn = X
  end do
  close(out_f)

end subroutine

program main
  implicit none
  integer :: re_i

  call solve_by_RKF45(0.0, 1.0)
  call solve_by_my_RK_method

  re_i = system("pause")
end program
